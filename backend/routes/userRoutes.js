const express = require('express')
const { getAllUsers, getUser, createNewUser, updateUser, deleteUser, getOrders, getAllOrders } = require('../controllers/userControllers')
const verifyJWT = require('../middlewares/verifyJWT')
const router = express.Router()

router.post('/', createNewUser)
router.use(verifyJWT)


router.get('/orders' ,getAllOrders)
router.get('/', getAllUsers)

router.get('/:id', getUser)
router.patch('/:id', updateUser)
router.delete('/:id' ,deleteUser)

router.get('/:id/orders', getOrders)



module.exports = router