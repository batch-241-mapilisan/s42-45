const express = require('express')
const { createProduct, getAllProducts, getAllActiveProducts, getProduct, updateProduct, archiveProduct, activateProduct, checkoutItems } = require('../controllers/productControllers')
const verifyJWT = require('../middlewares/verifyJWT')

const router = express.Router()

router.get('/active', getAllActiveProducts)
router.get('/', getAllProducts)
router.get('/:productId', getProduct)

router.use(verifyJWT)
router.post('/', createProduct)
router.patch('/:productId', updateProduct)
router.patch('/:productId/archive', archiveProduct)
router.patch('/:productId/activate', activateProduct)
router.post('/checkout', checkoutItems)

module.exports = router
