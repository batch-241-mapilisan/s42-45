const Product = require('../models/Product')
const User = require('../models/User')

// @desc Create Product
// @route POST /product
// @access Private (Admin only)
const createProduct = async (req, res) => {
    // Determine if user is an admin
    const isAdmin = req.isAdmin

    if (!isAdmin) {
        return res.status(400).json({ message: "Only admins can create a product" })
    }

    // Destructure data from request body
    const { name, description, price } = req.body

    // Verify data
    if (!name || !description || !price) {
        return res.status(400).json({ message: "All fields are required" })
    }

    // Check for duplicate
    const duplicate = await Product.findOne({ name: {$regex: `${name}`, $options: '$i'} })

    if (duplicate) {
        return res.status(400).json({ message: "Product already exists" })
    }

    // Create and store new product
    const productObject = { name, description, price }

    const newProduct = await Product.create(productObject)

    if (newProduct) {
        return res.status(200).json({ message: `New product ${name} successfully created` })
    } else {
        return res.status(400).json({ message: "Invalid product data received" })
    }
}

// @desc Get all products
// @route GET /products
// @access Public
const getAllProducts = async (req, res) => {
    const products = await Product.find()

    // Check if there are products
    if (!products.length) {
        return res.status(404).json({ message: "No products available" })
    }

    res.status(200).json(products)
}

// @desc Get all active products
// @route GET /products/active
// @access Public
const getAllActiveProducts = async (req, res) => {
    const activeProducts = await Product.find({ isActive: true })

    // Check if there are active products
    if (!activeProducts.length) {
        return res.status(404).json({ message: "There are no active products" })
    }

    res.status(200).json(activeProducts)
}

// @desc Get single product
// @route GET /products/:productId
// @access Public
const getProduct = async (req, res) => {
    // Destructure productId from request parameters
    const { productId } = req.params

    // Validate data
    if (!productId) {
        return res.status(400).json({ message: "Product Id is required" })
    }

    const product = await Product.findById(productId)

    // Check if product exists
    if (!product) {
        return res.status(404).json({ message: "Product does not exist" })
    }

    res.status(200).json(product)
}

// @desc Update product
// @route PATCH /products/:productId
// @access Private (Admin only)
const updateProduct = async (req, res) => {
    // Determine if user is an admin
    const isAdmin = req.isAdmin

    if (!isAdmin) {
        return res.status(400).json({ message: "Only admins can update a product" })
    }

    // Destructure productId from request parameter
    const { productId } = req.params

    // Destructure data from request body
    const { name, description, price } = req.body

    // Validate data
    if (!name || !description || !price) {
        return res.status(400).json({ message: "All fields are required" })
    }

    const product = await Product.findById(productId)

    // Check if product exists
    if (!product) {
        return res.status(404).json({ message: "Product does not exist" })
    }

    // Check for duplicate
    const duplicate = await Product.findOne({ name: {$regex: `${name}`, $options: '$i'} })
    if (duplicate && duplicate._id.toString() !== productId) {
        return res.status(400).json({ message: "Product already exists" })
    }

    // Update product
    product.name = name
    product.description = description
    product.price = price

    const updatedProduct = await product.save()

    res.status(200).json({ message: `Product ${updatedProduct.name} updated`, updatedProduct })
}

// @desc Archive product
// @route PATCH /products/:productId/archive
// @access Private (Admin only)
const archiveProduct = async (req, res) => {
    // Determine if user is an admin
    const isAdmin = req.isAdmin

    if (!isAdmin) {
        return res.status(400).json({ message: "Only admins can archive a product" })
    }

    // Destructure productId from request parameter
    const { productId } = req.params

    const product = await Product.findById(productId)

    // Check if product exists
    if (!product) {
        return res.status(404).json({ message: "Product does not exist" })
    }

    // Check if product is already archived
    if (!product.isActive) {
        return res.status(400).json({ message: "Product is already archived" })
    }

    product.isActive = false

    const archivedProduct = await product.save()

    res.status(200).json({ message: `Product ${archivedProduct.name} has been archived`, archivedProduct })
}

// @desc Activate product
// @route PATCH /products/:productId/activate
// @access Private (Admin only)
const activateProduct = async (req, res) => {
    // Determine if user is an admin
    const isAdmin = req.isAdmin

    if (!isAdmin) {
        return res.status(400).json({ message: "Only admins can activate a product" })
    }

    // Destructure productId from request parameter
    const { productId } = req.params

    const product = await Product.findById(productId)

    // Check if product exists
    if (!product) {
        return res.status(404).json({ message: "Product does not exist" })
    }

    // Check if product is already activated
    if (product.isActive) {
        return res.status(400).json({ message: "Product is already activated" })
    }

    product.isActive = true

    const activatedProduct = await product.save()

    res.status(200).json({ message: `Product ${activatedProduct.name} has been activated`, activatedProduct })
}

// @desc Checkout Items
// @route PATCH /products/checkout
// @access Private
const checkoutItems = async (req, res) => {
    // Destructure product Ids from request body
    const { productIds } = req.body
    

    // Validate data
    if (!productIds.length) {
        return res.status(400).json({ message: "There are no items in your cart" })
    }

    // Fetch user
    const userId = req.userId

    if (!userId) {
        return res.status(400).json({ message: "User must be logged in" })
    }

    const user = await User.findById(userId)

    // Fetch products in the cart
    let products = []
    let orderTotal = 0

    for (let i = 0; i < productIds.length; i++) {
        const product = await Product.findById(productIds[i])

        if (product) {
            const productObject = {
                productName: product.name,
                quantity: 1
            }
            orderTotal += product.price
            products.push(productObject)
        }
    }

    // Create order
    const newOrder = {
        products,
        totalAmount: orderTotal
    }

    user.orders = [...user.orders, newOrder]

    const updatedUser = await user.save()

    if (updatedUser) {
        return res.status(200).json({ message: "Order successfully placed", newOrder })
    } else {
        return res.status(400).json({ message: "Invalid user data received" })
    }
}

module.exports = {
    createProduct,
    getAllProducts,
    getAllActiveProducts,
    getProduct,
    updateProduct,
    archiveProduct,
    activateProduct,
    checkoutItems
}