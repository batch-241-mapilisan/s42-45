const User = require('../models/User')
const Product = require('../models/Product')
const bcrypt = require('bcrypt')

// @desc Get all users
// @route GET /users
// @access Private
const getAllUsers = async (req, res) => {
    // This query excludes the password field of every user from getting fetched back
    const users = await User.find().select('-password')

    // Check if there are existing users
    if (!users.length) {
        return res.status(404).json({ message: "No users found" })
    }

    res.status(200).json(users)
}

// @desc Get single user
// @route GET /users/:id
// @access Private
const getUser = async (req, res) => {
    // Destructure data from req params
    const { id } = req.params

    // Validate data
    if (!id) {
        return res.status(400).json({ message: "User Id required" })
    }

    const user = await User.findById(id).select('-password')

    // Check if user exists
    if (!user) {
        return res.status(400).json({ message: "User not found" })
    }

    res.status(200).json(user)
}

// @desc Create new user
// @route POST /users
// @access Public
const createNewUser = async (req, res) => {
    // Deconstruct data from request body
    const { email, password, firstName, lastName, isAdmin } = req.body 

    // Confirm data
    if (!email || !password || !firstName || !lastName) {
        return res.status(400).json({ message: "All fields are required" })
    }

    // Check for duplicate
    const duplicate = await User.findOne({ email })

    if (duplicate) {
        return res.status(409).json({ message: "Email is already used. Please use a different email" })
    }

    // Hash password
    const hashedPassword = await bcrypt.hash(password, 10)
    const userObject = (isAdmin)
                        ? { email, password: hashedPassword, firstName, lastName, isAdmin }
                        : { email, password: hashedPassword, firstName, lastName }

    // Create and store new user
    let newUser = await User.create(userObject)
    newUser = await User.findOne({email}).select('-password')

    if (newUser) {
        res.status(201).json({ message: `New user ${email} created`, newUser })
    } else {
        res.status(400).json({ message: "Invalid user data received" })
    }
}

// @desc Update user
// @route PATCH /users/:id
// @access Private
const updateUser = async (req, res) => {
    // Destructure id from request parameters
    const { id } = req.params

    // Destructure data from request body
    const { email, password, isAdmin, firstName, lastName } = req.body

    // Confirm data
    if (!id || !email || !firstName || !lastName) {
        return res.status(400).json({ message: "All fields are required" })
    }

    
    const user = await User.findById(id)

    // Check if user exists
    if (!user) {
        return res.status(400).json({ message: "User not found" })
    }

    // Check for duplicate
    const duplicate = await User.findOne({ email })

    if (duplicate && duplicate._id.toString() !== id) {
        return res.status(409).json({ message: "Email is already used. Please use a different email" })
    }

    // Check if user is admin
    // If user is admin it can modify isAdmin property of any user, if not isAdmin property will always be set to false
    const isUserAdmin = req.isAdmin

    if (!isUserAdmin && isAdmin) {
        return res.status(400).json({ message: "You need to be an admin to set admin status" })
    }
    
    user.email = email
    user.firstName = firstName
    user.lastName = lastName
    user.isAdmin = isAdmin

    if (password) {
        user.password = await bcrypt.hash(password, 10)
    }

    const updatedUser = await user.save()

    res.status(200).json({ message: `User ${updatedUser.email} updated`, updatedUser })
}

// @desc Delete user
// @route DELETE /users/:id
// @access Private
const deleteUser = async (req, res) => {
    // Destructure id from request parameters
    const { id } = req.params

    // Validate data
    if (!id) {
        return res.status(400).json({ message: "User Id required" })
    }

    // Get user data from token
    const userId = req.userId
    const isAdmin = req.isAdmin

    // Check if id from token matches with id
    if (id !== userId && !isAdmin) {
        return res.status(400).json({ message: "You cannot delete other user's account" })
    }

    // Check if user exists
    const user = await User.findById(id)

    if (!user) {
        return res.status(404).json({ message: "User not found" })
    }

    const result = await user.deleteOne()

    const reply = `User ${result.email} deleted`

    res.status(200).json(reply)
}

// @desc Retrieve Orders
// @route GET /users/:id/orders
// @access Private
const getOrders = async (req, res) => {
    // Get user information from token
    const userId = req.userId

    // Destructure id from request parameters
    const { id } = req.params

    // Check if id matches
    if (id !== userId) {
        return res.status(400).json({ message: "You cannot retrieve other user's orders" })
    }

    const user = await User.findById(id)

    if (!user) {
        return res.status(404).json({ message: "User not found" })
    }

    res.status(200).json(user.orders)
}

// @desc Get All Orders
// @route GET users/orders
// @access Private (admin only)
const getAllOrders = async (req, res) => {
    // Check if user is an admin
    const isAdmin = req.isAdmin

    if (!isAdmin) {
        return res.status(400).json({ message: "Only admin can get all user's orders" })
    }

    const users = await User.find()

    if (!users.length) {
        return res.status(404).json({ message: "No users found" })
    }

    const userOrders = users.map(user => {
        return {
            userId: user._id,
            orders: [user.orders]
        }
    })

    res.status(200).json(userOrders)
}

module.exports = {
    getAllUsers,
    getUser,
    createNewUser,
    updateUser,
    deleteUser,
    getOrders,
    getAllOrders
}

