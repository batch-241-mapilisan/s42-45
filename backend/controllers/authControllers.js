const User = require('../models/User')
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')

// @desc - Login
// @route POST /auth
// @access Public
const login = async (req, res) => {
    // Destructure data from request body
    const { email, password } = req.body

    // Verify data
    if (!email || !password) {
        return res.status(400).json({ message: "All fields are required "})
    }

    // Check if user exists
    const foundUser = await User.findOne({ email })

    if (!foundUser) {
        return res.status(401).json({ message: "User is not found" })
    }

    // Check if password matches
    const match = await bcrypt.compare(password, foundUser.password)

    if (!match) {
        return res.status(401).json({ message: "Invalid credentials" })
    }

    // Create accessToken and attach user data into it
    const accessToken = jwt.sign(
        {
            "UserInfo": {
                "id": foundUser._id,
                "email": foundUser.email,
                "isAdmin": foundUser.isAdmin
            }
        },
        process.env.ACCESS_TOKEN_SECRET,
        { expiresIn: '1d' }
    )

    // Send accessToken containing user data
    res.json({accessToken})
}

module.exports = {
    login
}