// Declaring dependencies
require('dotenv').config()
const express = require('express')
const mongoose = require('mongoose')
const cors = require('cors')
const connectDb = require('./config/connectDb')
const userRoutes = require('./routes/userRoutes')
const authRoutes = require('./routes/authRoutes')
const productRoutes = require('./routes/productRoutes')

const app = express()
const PORT = process.env.PORT || 4000

// Connect to Database
connectDb()

// Middlewares
app.use(cors())
app.use(express.urlencoded({ extended: false }))
app.use(express.json())

// Routes
app.use('/auth', authRoutes)
app.use('/users', userRoutes)
app.use('/products', productRoutes)

const db = mongoose.connection

db.once('open', () => {
    console.log('Connected to MongoDB')
    app.listen(PORT, () => console.log(`Server is up and running on port ${PORT}`))
})

db.on('error', error => {
    console.log(error)
})
