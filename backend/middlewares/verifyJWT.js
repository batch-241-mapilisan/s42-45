const jwt = require('jsonwebtoken')

const verifyJWT = (req, res, next) => {
    const authHeader = req.headers.authorization || req.headers.Authorization

    // Check if Auth header exists
    if (!authHeader.startsWith('Bearer ')) {
        return res.status(401).json({ message: "Unauthorized" })
    }

    // Extract token from auth header
    const token = authHeader.split(' ')[1]

    jwt.verify(
        token,
        process.env.ACCESS_TOKEN_SECRET,
        (error, decoded) => {
            if (error) {
                return res.status(403).json({ message: "Forbidden" })
            }
            req.userId = decoded.UserInfo.id
            req.email = decoded.UserInfo.email
            req.isAdmin = decoded.UserInfo.isAdmin
            next()
        }
    )
}

module.exports = verifyJWT